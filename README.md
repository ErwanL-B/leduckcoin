# LEDUCKCOIN

## Projet DevPRO

---

## Caractéristique du projet

Langage choisi : NodeJs

Base de données choisie : MongoDB

Langue choisie de programmation lors du codage : ANGLAIS

Gestionnaire de sources choisi : GIT (GitLab)

Thème de l'API : copie du site Leboncoin

Objectifs du site Leduckcoin :

- Visiter des annonces de canards
- Créer un compte leduckcoin
- Gérer des annonces de canards

Objectifs techniques :

- Créer une application sous forme de micro-service comprenant les éléments suivants :
  - Une API REST
  - Une interface utilisateur (IHM) intuitive
  - Un modèle de données persistant dans une base de données

Mes objectifs personnels pour ce projet :

- Réaliser une API REST en utilisant le langage Node.js (POC)
- Traduire l'API REST NodeJs en langage Rust
- Créer une interface utilisateur React
- Mettre en place un serveur d'accès pour une gestion sécurisée de la base de données
- Implémenter une API prenant en charge la gestion des cookies
- Mettre en place une API utilisant le protocole HTTPS
- Réaliser un serveur d'accès incluant un système de Log
- Développer un projet contenant des conteneurs Docker
- Mettre en place des méthodes de suivi et de déploiement dans le projet
- Développer un projet de manière efficace avec Git

---

## Ce qui a été réalisé avec succès

### Objectifs du projet

**IHM intuifif** : Atteint.

L'IHM est simple et intuitive. Aucune documentation de navigation n'est nécessaire pour utiliser le site.

**API REST** : Atteint.

  * **Gestion des offres** : Partiellement atteint.

L'API permet de créer des offres, de les lister pour tous les utilisateurs ainsi que de les gérer pour son propriétaire.
Un utilisateur peut créer une offre, la visualiser ainsi que la supprimer.
La modification des offres est manquante.

  * **Gestion des utilisateurs** : Atteint.

L'API permet de créer un nouvel utilisateur, de se connecter ainsi que de se déconnecter.
Les informations utilisateur ne sont jamais divulgées, aucun mot de passe n'est enregistré sans être crypté.
La connexion d'un utilisateur utilise un système de token permettant à un utilisateur de maintenir une session active.

**Modèle de donnée persistant** : Atteint.

Une base de données MongoDB est mise en place dans ce projet.
La base de données est accessible par le serveur d'accès et permet de gérer les offres ainsi que les utilisateurs.
Une méthode de persistance est mise en place afin de garder les données de la base, même lorsque le conteneur Docker est stoppé.

### Objectifs personnels

**Réalisation d'une API REST (NodeJs)** : Atteint.

L'API est une API REST qui prend en charge les requêtes GET, POST et DELETE.
Les réponses aux requêtes sont au format JSON afin de faciliter la gestion des réponses.

**Réalisation d'une interface utilisateur React** : Atteint.

L'interface utilisateur est interactive et fluide. Grâce à React, la page du navigateur n'est pas actualisée à chaque interaction.

**Réalisation d'une API incluant la gestion des cookies** : Atteint.

L'interface utilisateur intègre la gestion des cookies afin de maintenir l'authentification de l'utilisateur active.
Les cookies sont gérés grâce à des bibliothèques NodeJs.

**Réalisation d'une API utilisant le protocole HTTPS** : Atteint.

L'API REST utilise le protocole HTTPS pour les communications.
Des certificats self-signed sont utilisés pour établir la communication sécurisée.

**Réalisation d'un serveur d'accès à la base de données** : Atteint.

L'API REST inclut une partie backend (serveur d'accès) permettant l'échange d'informations entre l'interface utilisateur et la base de données.
Le serveur d'accès gère :
  - L'authentification de l'utilisateur
  - La gestion des erreurs
  - La gestion des réponses vers l'interface utilisateur
  - La gestion des Logs
  - La gestion des données dans la base de données (vérification des doublons)

**Réalisation d'un projet incluant Docker** : Atteint.

Ce projet a été développé en utilisant Docker pour permettre le déploiement efficace de l'application.
Trois images Dockers sont utilisées :
  - L'interface utilisateur
  - Le serveur d'accès
  - La base de données

Chaque image Dockers représente un micro-service.

**Réalisation d'un projet incluant des méthodes de déploiement** : Partiellement atteint.

Ce projet a été développé en incluant des méthodes de déploiement.
Pour vérifier et déployer le projet, Jenkins est utilisé.
Jenkins permet de vérifier la compilation de l'interface utilisateur ainsi que du serveur d'accès.
Une fois compilés avec succès, Jenkins créer une image Docker des deux programmes, puis les importe sur GitLab pour les versionner.
Un conteneur sauvegardé sur GitLab peut être déployé à l'aide d'outils tels que Ansible. Cependant cette partie n'a pas été réalisée avec succès.
Pour permettre le déploiement de ce projet, un ensemble Dockerfile - docker-compose.yml est présent dans le dossier 'deploiement', permettant l'importation et l'exécution des conteneurs.
Le déploiement n'est donc pas automatisé, une manipulation humaine est requise.

**Réalisation d'un projet incluant une gestion avec Git** : Atteint.

Ce projet a été développé et versionné grâce à Git (GitLab).
Deux branches ont été créées : la branche principale (Main) et une branche de développement (Dev).  La branche Dev permet de développer l'application avant de la fusionner dans la branche Main.

---

## Ce qui n'a pas été réalisé

### Objectifs du projet

**Modification des offres** : Non atteint.

Pour permettre la modification des offres, des requêtes PUT auraient dû être mises en place.
En raison d'un manque de temps et d'une mauvaise gestion du projet, cette fonctionnalité n'a pas pu être achevée.
Le serveur d'accès a été partiellement développé pour inclure la modification des offres.

### Objectifs personnels

**Réalisation d'un server d'accès sécurisé** : Echec partiel.

Aucun sécurisation n'est mise en place entre le serveur d'accès et la base de données.
Le système de Logs ne masque pas les mots de passe des utilisateurs.
Un début de développement ajoutant des comptes à la base de données a été effectué, cependant, le développement n'est pas concluant.
La communication entre l'interface utilisateur et le serveur d'accès est sécurisée via le protocole HTTPS.
Les mots de passe utilisateurs sont cryptés par le serveur d'accès avant d'être sauvegardés dans la base de données.

**Réalisation d'une API REST (Rust)** : Non atteint.

Dans le but d'apprendre un nouveau langage et de maximiser la sécurité du serveur d'accès, il a été décidé, lors de la phase de conception, que le serveur d'accès serait développé en Rust.
Un POC du serveur d'accès a été réalisé en NodeJs pour faciliter le développement des autres micro-services.
Le serveur d'accès devait être traduit en langage Rust afin de :
  - Devenir plus léger en utilisant moins de bibliothèques
  - Utiliser des outils natifs pour inclure des méthodes de sécurisation des données
  - Gagner en sécurité en évitant les comportements aléatoires

En raison du manque de temps, la traduction n'a pas pu être réalisée et l'objectif personnel n'a pas été atteint.
Une infrastructure de déploiement pour les programmes Rust a été mise en place avec succès.
Seul le serveur d'accès en Rust manquant pour atteindre cet objectif.

**Réalisation d'une gestion de projet** : Non atteint.

Aucune gestion de projet n'a été mise en place pour ce projet.
Une méthode de gestion de projet aurait permis une meilleure planification des méthode de déploiement ainsi qu'une meilleur gestion du temps et des objectifs.
Travaillant seul, il est facile de négliger la gestion de projet et de développer sur le fil des idées.

**Accès à la base de données sécurisé** : Non atteint.

Aucune méthode de sécurisation n'a été mise en place pour l'accès à la base de données.
Aucun compte n'est configuré pour accéder à la base de données.

---

### Contribution

- Jonathan Garriguet (Aide sur la réalisation de la documentation)
- François Sidérikoudis (Aide sur le débuggage de la Pipeline)

---

### Avis sur le projet

Ce projet a été très intéressant notamment en ce qui concerne la partie déploiement. N'ayant jamais réalisé de déploiement auparavant, ce projet était une première pour moi.
Malgré certaines difficulté lors de l'utilisation de l'outil Jenkins, j'ai pu mettre en place une pipeline permettant le déploiement partiel du projet.

Je ne suis pas vraiment satisfait de mon travail en se qui concerne le développement du serveur d'accès en langage Rust.
J'aurai aimé réaliser le POC plus rapidement afin de disposer de plus de temps pour apprendre le langage Rust et développer la version finale du serveur d'accès.
La création d'un dispositif de déploiement pour le langage Rust a été très instructive, cela m'a permis de le mettre en place plus facilement pour le langage Node.js.

En ce qui concerne la partie de développement, des améliorations doivent être apportées en termes de gestion de projet.
Une meilleure organisation de mon planning pourrait me permettre d'atteindre tous mes objectifs personnels.

Auparavant, j'ai eu réalisé une application REST en langage NodeJs, incluant React, dans l'objectif de découvrir le langage et le framework.
Ce projet antérieur m'a permit d'apprendre les bases du langage NodeJs, ce qui m'a amené à décider, en phase de conception de ce projet, la réalisation d'un POC en NodeJs. Ce projet m'a permis d'approfondir mes connaissances et mes compétences dans ce domaine.
