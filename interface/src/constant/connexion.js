import https from 'https';

//const DEFAULT_ADDRESS = 'https://localhost';
const DEFAULT_ADDRESS = 'https://10.20.1.63';
const DEFAULT_PORT = '8845';

export const axios_option_GET = {
  method: 'GET',
  baseURL: DEFAULT_ADDRESS + ':' + DEFAULT_PORT,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
};

export const axios_option_POST = {
  method: 'POST',
  headers: { 'Content-type': 'multipart/form-data; charset=UTF-8' },
  baseURL: DEFAULT_ADDRESS + ':' + DEFAULT_PORT,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
};

export const axios_option_DELETE = {
  method: 'DELETE',
  baseURL: DEFAULT_ADDRESS + ':' + DEFAULT_PORT,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
};
