export const ROUTE_SIGNUP = '/api/sign_up';
export const ROUTE_LOGIN = '/api/log_in';
export const ROUTE_PROFILE = '/api/profile';
export const ROUTE_PUBLISH = '/api/send_offer';
export const ROUTE_OFFERS = '/api/offers';
export const ROUTE_OFFER = '/api/offer';
export const ROUTE_DELETE = '/api/delete';
