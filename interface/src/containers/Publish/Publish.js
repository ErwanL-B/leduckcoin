import React, { Component } from 'react';
import axios from 'axios';
import ReactFileReader from 'react-file-reader';
import Loading from '../../components/Loading/Loading';
import { axios_option_POST } from '../../constant/connexion.js';

import { ROUTE_PUBLISH } from '../../constant/routes';

import './Publish.css';

class Publish extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      description: '',
      price: '',
      image: [],
      type: '',
      gender: '',
      color: '',

      isLoading: false,
    };
  }

  handleChangePrice = (event) => {
    this.setState({
      [event.target.name]: event.target.value.replace(/[^0-9]/g, ''),
    });
  };

  handleImage = (image) => {
    const { image: stateImage } = this.state;
    const newImage = [...stateImage];
    newImage.push(image.base64);
    this.setState({
      image: newImage,
    });
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  submitForm = async (event) => {
    const { title, description, price, image, type, color, gender } =
      this.state;
    const { getUser } = this.props;

    event.preventDefault();
    try {
      const { token } = getUser();
      this.setState({
        isLoading: true,
      });
      let option = axios_option_POST;

      let form = new FormData();
      form.append('image', image);
      option.data = form;

      option.params = {
        token,
        title,
        description,
        price: Number(price),
        type,
        color,
        gender,
      };

      await axios(ROUTE_PUBLISH, option).then(() => {
        this.setState({
          title: '',
          description: '',
          price: '',
          image: '',
          type: '',
          gender: '',
          color: '',

          isLoading: false,
        });
      });
    } catch (error) {
      console.error({
        error: error.message,
        specific: 'The ad was not published',
      });
    }
  };

  renderButton = () => {
    const { isLoading } = this.state;

    if (!isLoading) {
      return (
        <button type="submit" className="validate-btn">
          Valider
        </button>
      );
    }
    return (
      <>
        <Loading />
        <span data-testid="loading" className="loading-message">
          <span role="img" aria-label="smily face"></span>
        </span>
      </>
    );
  };

  render() {
    const { title, description, price, image, type, gender, color } =
      this.state;

    const imageOnClick = () => {
      const newImage = [...image];
      this.setState({
        image: newImage,
      });
    };

    const imageOffer = () => {
      if (image.length > 0) {
        return (
          <img
            data-testid="image"
            key="1"
            onClick={() => imageOnClick()}
            src={image}
            alt="annonce"
          />
        );
      } else {
        return <div>Image principale</div>;
      }
    };

    return (
      <div className="wrapper">
        <div className="ad-listing">
          <h1>Déposer une annonce</h1>

          <div className="ad-listing-container">
            <h2>Votre annonce</h2>
            <form data-testid="submit-form" onSubmit={this.submitForm}>
              <div className="ad-listing-body">
                <label htmlFor="title">Titre de l&apos;annonce</label>
                <input
                  id="title"
                  data-testid="input-title"
                  type="text"
                  name="title"
                  value={title}
                  maxLength="50"
                  onChange={this.handleChange}
                  required
                />
                <label htmlFor="description">Texte de l&apos;annonce</label>
                <textarea
                  id="description"
                  data-testid="textarea-description"
                  rows="10"
                  name="description"
                  value={description}
                  maxLength="4000"
                  onChange={this.handleChange}
                />
                <label htmlFor="title">Type</label>
                <input
                  id="type"
                  data-testid="input-type"
                  type="text"
                  name="type"
                  value={type}
                  maxLength="50"
                  onChange={this.handleChange}
                />
                <label htmlFor="title">Genre</label>
                <input
                  id="gender"
                  data-testid="input-gender"
                  type="text"
                  name="gender"
                  value={gender}
                  maxLength="50"
                  onChange={this.handleChange}
                />
                <label htmlFor="title">Couleur</label>
                <input
                  id="color"
                  data-testid="input-color"
                  type="text"
                  name="color"
                  value={color}
                  maxLength="50"
                  onChange={this.handleChange}
                />
                <label htmlFor="price">Prix</label>
                <input
                  id="price"
                  data-testid="input-price"
                  type="text"
                  name="price"
                  value={price}
                  maxLength="8"
                  onChange={this.handleChangePrice}
                  required
                />
                <p>
                  <span>Photo :</span>
                </p>
                <aside>
                  <ReactFileReader
                    fileTypes={['.png', '.jpg']}
                    base64
                    multiplesFiles={false}
                    handleFiles={this.handleImage}
                  >
                    <div className="box-photo">{imageOffer()}</div>
                  </ReactFileReader>
                </aside>
                {this.renderButton()}
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Publish;
