import React, { Component } from 'react';
import axios from 'axios';

import Card from '../../components/Card/Card';
import Loading from '../../components/Loading/Loading';
import {
  axios_option_POST,
  axios_option_DELETE,
} from '../../constant/connexion.js';

import { ROUTE_PROFILE, ROUTE_DELETE } from '../../constant/routes';

import './Profile.css';

class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      offers: [],
      isDelete: true,

      isLoading: true,
      error: null,
    };
  }

  async componentDidMount() {
    this.fetchOffer();
  }

  fetchOffer = async () => {
    const { getUser } = this.props;

    try {
      const { token } = getUser();

      let option = axios_option_POST;
      option.params = {
        token,
      };
      const response = await axios(ROUTE_PROFILE, option);

      const offer = response.data;
      this.setState({
        offers: offer,
        isLoading: false,
      });
    } catch (error) {
      console.error(error);
    }
  };

  deleteOffer = async (id) => {
    const { getUser } = this.props;

    try {
      const { token } = getUser();

      let option = axios_option_DELETE;
      option.params = {
        token,
        id,
      };
      await axios(ROUTE_DELETE, option).then(() => this.fetchOffer());
    } catch (error) {
      console.error('not deleted', error);
    }
  };

  renderOffers() {
    const { isLoading, error, offers, isDelete } = this.state;

    if (error || (!isLoading && !offers)) {
      return null;
    }

    if (isLoading) {
      return <Loading />;
    }

    return (
      <div className="wrapper homepage">
        <ul>
          {offers.map(
            ({ id, title, image, description, price, created_at }) => (
              <Card
                // dataTestId="profile-ad-card"
                isDelete={isDelete}
                deleteOffer={this.deleteOffer}
                key={id + title}
                image={image}
                id={id}
                title={title}
                description={description}
                price={price}
                created_at={created_at}
              />
            )
          )}
        </ul>
      </div>
    );
  }

  render() {
    const { getUser } = this.props;

    return (
      <>
        <div className="wrapper">
          <div className="ad-listing">
            <h1>Votre profil</h1>
            <div className="profile-details">
              <p>Pseudo : {getUser().username}</p>
              <p>Email : {getUser().email}</p>
            </div>
            {this.renderOffers()}
          </div>
        </div>
      </>
    );
  }
}

export default Profile;
