import React, { Component } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';

import { axios_option_GET } from '../../constant/connexion.js';

import Filters from '../../components/Filters/Filters';
import Card from '../../components/Card/Card';

import Pagination from '../../components/Pagination/Pagination';
import Loading from '../../components/Loading/Loading';

import './Offers.css';
import { ROUTE_OFFERS } from '../../constant/routes.js';

class Offers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      offers: [],
      limit: 10, // Products per page
      totalPages: 1,
      currentPage: 1,

      title: Cookies.get('title') || '',
      minPrice: Cookies.get('minPrice') || '',
      maxPrice: Cookies.get('maxPrice') || '',
      sort: Cookies.get('sort') || '',

      isLoading: true,
      error: null,
    };
  }

  async componentDidMount() {
    try {
      await axios(ROUTE_OFFERS, axios_option_GET).then((response) => {
        const { limit } = this.state;
        const offers = response.data;
        this.setState({
          offers,
          totalPages: Math.ceil(parseFloat(offers.length / limit)),
        });
        this.goToPage(1);
      });
    } catch (error) {
      this.setState({
        error,
      });
    }
  }

  async refreshTotalPages() {
    try {
      await axios(ROUTE_OFFERS, axios_option_GET).then((response) => {
        const { limit } = this.state;
        const offers = response.data;
        this.setState({
          totalPages: Math.ceil(parseFloat(offers.length / limit)),
        });
      });
    } catch (error) {
      this.setState({
        error,
      });
    }
  }

  goToPage = async (pageClicked) => {
    const { title, minPrice, maxPrice, sort, limit } = this.state;
    const skip = (pageClicked - 1) * limit;
    try {
      if (title !== '' || minPrice !== '' || maxPrice !== '' || sort !== '') {
        const criteria = {
          title,
          minPrice,
          maxPrice,
          sort,
        };

        this.searchFilters(criteria);
        this.setState({
          isLoading: false,
        });
      } else {
        await axios(
          `${ROUTE_OFFERS}?skip=${skip}&limit=${limit}`,
          axios_option_GET
        ).then((response) => {
          const offers = response.data;
          this.setState({
            offers,
            currentPage: pageClicked,
            isLoading: false,
          });
          this.refreshTotalPages();
        });
      }
    } catch (error) {
      this.setState({
        error: 'An error occurred',
      });
    }
  };

  handleFilters = (event) => {
    const { name } = event.target;
    const { value } = event.target;
    const stateToUpdate = {};

    if (value === '') {
      Cookies.remove('title');
      Cookies.remove('minPrice');
      Cookies.remove('maxPrice');
      Cookies.remove('sort');
    }

    stateToUpdate[name] = value;

    this.setState(stateToUpdate);
  };

  searchFilters = async (criteria) => {
    try {
      if (criteria !== undefined) {
        let maxPriceQuery = '';
        if (criteria.maxPrice !== '') {
          maxPriceQuery = `&priceMax=${criteria.maxPrice}`;
        }

        await axios(
          `${ROUTE_OFFERS}?title=${criteria.title}&priceMin=${criteria.minPrice}${maxPriceQuery}&sort=${criteria.sort}`,
          axios_option_GET
        ).then((response) => {
          const offers = response.data;

          const { title, minPrice, maxPrice, sort } = this.state;

          this.setState({
            offers,
            title,
            minPrice,
            maxPrice,
            sort,
            totalPages: 1,
          });

          Cookies.set('title', title);
          Cookies.set('minPrice', minPrice);
          Cookies.set('maxPrice', maxPrice);
          Cookies.set('sort', sort);
        });
      }
    } catch (error) {
      this.setState({
        error: 'An error occured',
      });
    }
  };

  submitFilters = (event) => {
    event.preventDefault();
    const { title, minPrice, maxPrice, sort } = this.state;

    const criteria = {
      title,
      minPrice,
      maxPrice,
      sort,
    };

    const keys = Object.values(criteria);

    // Checks if a value exists
    const notEmpty = keys.filter((value) => value !== '').length > 0;

    if (notEmpty) {
      this.searchFilters(criteria);
    } else {
      this.searchFilters();
      this.setState({
        title: '',
        minPrice: '',
        maxPrice: '',
        sort: '',
      });
      Cookies.remove('title');
      Cookies.remove('minPrice');
      Cookies.remove('maxPrice');
      Cookies.remove('sort');

      this.goToPage(1);
    }
  };

  renderMain() {
    const { isLoading, error, currentPage, totalPages, offers } = this.state;
    const { windowWidth } = this.props;

    if (error !== null) return null;
    if (isLoading) return <Loading />;
    if (!offers) return <div>No offers at the moment..</div>;

    return (
      <>
        <div className="wrapper Offerspage">
          <ul>
            {Array.isArray(offers)
              ? offers.map(
                  ({ id, image, title, description, price, created_at }) => (
                    <Card
                      key={id + title + created_at}
                      image={image}
                      id={id}
                      title={title}
                      description={description}
                      price={price}
                      created_at={created_at}
                    />
                  )
                )
              : null}
          </ul>
        </div>
        <Pagination
          currentPage={currentPage}
          totalPages={totalPages}
          goToPage={this.goToPage}
          windowWidth={windowWidth}
        />
      </>
    );
  }

  render() {
    const { title, minPrice, maxPrice, sort } = this.state;

    return (
      <>
        <Filters
          title={title}
          minPrice={minPrice}
          maxPrice={maxPrice}
          sort={sort}
          handleFilters={this.handleFilters}
          submitFilters={this.submitFilters}
        />

        {this.renderMain()}
      </>
    );
  }
}

export default Offers;
