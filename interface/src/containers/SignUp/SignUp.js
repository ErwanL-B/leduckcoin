import React, { Component } from 'react';
import axios from 'axios';
import { ReactComponent as ClockIcon } from '../../assets/img/clock.svg';
import { ReactComponent as BellIcon } from '../../assets/img/bell.svg';
import { ReactComponent as EyeIcon } from '../../assets/img/eye.svg';
import { axios_option_POST } from '../../constant/connexion.js';
//import bcrypt from 'bcrypt';

import { ROUTE_SIGNUP } from '../../constant/routes';

import './SignUp.css';

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      password: '',
      confirmPassword: '',
      connexionText: '',
      phone: '',
    };
  }

  handleChange = (event) =>
    this.setState({
      [event.target.name]: event.target.value,
    });

  handleChangePhone = (event) => {
    this.setState({
      [event.target.name]: event.target.value.replace(/[^0-9]/g, ''),
    });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const { username, email, password, confirmPassword, phone } = this.state;

      if (username && email && password && confirmPassword) {
        if (password === confirmPassword) {
          let option = axios_option_POST;
          option.params = {
            email,
            username,
            password,
          };
          if (phone) option.params.phone = phone;
          await axios(ROUTE_SIGNUP, option).then((response) => {
            if (response.data) {
              try {
                this.props.setUser(response.data);
                const message = (
                  <div
                    className="flex"
                    style={{
                      margin: 10,
                      color: '#00ff00',
                    }}
                  >
                    <p>Compte Ajouté</p>
                  </div>
                );

                this.setState({
                  isValid: true,
                  connexionText: message,
                });
              } catch (error) {
                console.error(error.message);
              }
            }
          });
        } else {
          console.log('the passwords are not similar');
        }
      } else {
        console.log("you're missing one field");
      }
    } catch (error) {
      const message = (
        <div
          className="flex"
          style={{
            margin: 10,
            color: '#ff0000',
          }}
        >
          <p>{error.response.data.message}</p>
        </div>
      );
      this.setState({
        connexionText: message,
      });
      console.error(error.message);
    }
  };

  render() {
    const { username, email, password, confirmPassword, phone, connexionText } =
      this.state;

    return (
      <div className="wrapper">
        <div className="sign-up-page">
          <div className="sign-up-flex">
            <div className="sign-up-flex-left">
              <section>
                <h2>Pourquoi créer un compte ?</h2>
                <div className="flex-row speed">
                  <div>
                    <ClockIcon />
                  </div>
                  <div className="flex-column">
                    <p>Gagnez du temps</p>
                    <p>
                      Publiez vos annonces rapidement, avec vos informations
                      pré-remplies chaque fois que vous souhaitez déposer une
                      nouvelle annonce.
                    </p>
                  </div>
                </div>
                <div className="flex-row">
                  <div>
                    <BellIcon />
                  </div>
                  <div className="flex-column">
                    <p>Soyez les premiers informés</p>
                    <p>
                      Créez des alertes Immo ou Emploi et ne manquez jamais
                      l&quot;annonce qui vous intéresse.
                    </p>
                  </div>
                </div>
                <div className="flex-row">
                  <div>
                    <EyeIcon />
                  </div>
                  <div className="flex-column">
                    <p>Visibilité</p>
                    <p>
                      Suivez les statistiques de vos annonces (nombre de fois où
                      votre annonce a été vue, nombre de contacts reçus).
                    </p>
                  </div>
                </div>
              </section>
            </div>
            <div className="sign-up-flex-right connection">
              <section>
                <h1>Créez un compte</h1>
                <form data-testid="submit-form" className="form">
                  <div className="form-item">
                    <label htmlFor="username">pseudo *</label>
                    <input
                      id="username"
                      data-testid="input-username"
                      type="text"
                      name="username"
                      value={username}
                      onChange={this.handleChange}
                      required
                    />
                  </div>

                  <div className="form-item">
                    <label htmlFor="email">Adresse email *</label>
                    <input
                      id="email"
                      data-testid="input-email"
                      type="email"
                      name="email"
                      value={email}
                      onChange={this.handleChange}
                      required
                    />
                  </div>

                  <div className="form-item">
                    <label htmlFor="phone">Phone</label>
                    <input
                      id="phone"
                      data-testid="input-phone"
                      type="tel"
                      name="phone"
                      value={phone}
                      onChange={this.handleChangePhone}
                    />
                  </div>

                  <div className="form-item form-password">
                    <div className="form-item">
                      <label htmlFor="password">Mot de passe *</label>
                      <input
                        id="password"
                        data-testid="input-password"
                        type="password"
                        name="password"
                        value={password}
                        onChange={this.handleChange}
                        required
                      />
                    </div>
                    <div className="form-item">
                      <label htmlFor="confirmPassword">
                        Confirmer le mot de passe *
                      </label>
                      <input
                        id="confirmPassword"
                        data-testid="input-confirm-password"
                        type="password"
                        name="confirmPassword"
                        value={confirmPassword}
                        onChange={this.handleChange}
                        required
                      />
                    </div>
                  </div>
                  <p>{connexionText}</p>
                  <div className="form-checkbox">
                    <input type="checkbox" name="newsletter" id="newsletter" />
                    <label htmlFor="newsletter">
                      Je souhaite recevoir des offres des partenaires du site
                      leduckcoin susceptibles de m&quot;intéresser
                    </label>
                  </div>

                  <div className="form-checkbox">
                    <input
                      data-testid="checkbox-legal"
                      type="checkbox"
                      name="legal"
                      required
                    />
                    <label htmlFor="legal">
                      &quot;J&rsquo;accepte les&nbsp;
                      <span>Conditions Générales de Vente&quot;</span>
                    </label>
                  </div>

                  <button type="button" onClick={this.handleSubmit}>
                    Créer mon Compte Personnel
                  </button>
                </form>
              </section>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SignUp;
