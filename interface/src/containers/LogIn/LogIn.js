import React, { Component } from 'react';
import axios from 'axios';

import { ROUTE_LOGIN, ROUTE_SIGNUP } from '../../constant/routes';
import { axios_option_POST } from '../../constant/connexion.js';

import './LogIn.css';

class LogIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      connexionText: '',
      isValid: true,
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = async (event) => {
    const { email, password } = this.state;

    event.preventDefault();
    try {
      let option = axios_option_POST;

      option.params = { email: email, password: password };
      await axios(ROUTE_LOGIN, option).then((response) => {
        this.setState({
          isValid: false,
        });

        if (response.data.token) {
          this.props.setUser(response.data);

          const message = (
            <div
              className="flex"
              style={{
                margin: 10,
                color: '#00ff00',
              }}
            >
              <p>Connexion réussi</p>
            </div>
          );

          this.setState({
            isValid: true,
            connexionText: message,
          });
        }
      });
    } catch (error) {
      const message = (
        <div
          className="flex"
          style={{
            margin: 10,
            color: '#ff0000',
          }}
        >
          <p>{error.response.data.message}</p>
        </div>
      );
      this.setState({
        connexionText: message,
      });
    }
  };

  render() {
    const { history } = this.props;
    const { email, password, isValid, connexionText } = this.state;

    return (
      <div className="wrapper connection">
        <div className="sign-in-flex">
          <section>
            <h1>Connexion</h1>
            <form
              data-testid="login"
              onSubmit={this.handleSubmit}
              className="form"
            >
              <div className="form-item">
                <label htmlFor="email">Adresse email</label>
                <input
                  data-testid="input-email"
                  type="email"
                  name="email"
                  value={email}
                  onChange={this.handleChange}
                  required
                />
              </div>

              <div className="form-item">
                <div className="form-item">
                  <label htmlFor="password">Mot de passe</label>
                  <input
                    data-testid="input-password"
                    style={{
                      boxShadow: !isValid && '0 0 0 3px red inset',
                    }}
                    type="password"
                    name="password"
                    value={password}
                    onChange={this.handleChange}
                    required
                  />
                </div>
              </div>
              <p>{connexionText}</p>
              <button type="submit">Se connecter</button>
            </form>

            <div className="create-account-redirect">
              <p>Vous n&apos;avez pas de compte ?</p>
              <button
                type="button"
                data-testid="go-to-signup"
                className="btn"
                onClick={() => history.push(ROUTE_SIGNUP)}
              >
                Créer un compte
              </button>
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default LogIn;
