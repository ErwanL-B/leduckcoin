import React, { Component } from 'react';
import axios from 'axios';
import { format } from 'date-fns';
import { axios_option_GET } from '../../constant/connexion.js';

import Loading from '../../components/Loading/Loading';

import './Offer.css';
import { ROUTE_OFFER } from '../../constant/routes.js';

class Offer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      offer: {},
      isLoading: true,
      error: null,
    };
  }

  async componentDidMount() {
    try {
      const {
        match: { params },
      } = this.props;

      await axios(`${ROUTE_OFFER}?id=${params.id}`, axios_option_GET).then(
        (response) => {
          const offer = response.data;

          this.setState({
            offer,
            isLoading: false,
          });
        }
      );
    } catch (error) {
      this.setState({
        error: 'An error occurred',
      });
    }
  }

  renderSection() {
    const {
      isLoading,
      error,
      offer: { title, price, description, created_at, creator, image },
    } = this.state;

    if (error) return null;

    if (isLoading) return <Loading />;

    const imageStyle = { width: '100%', height: '100%' };

    const imageOffer = () => {
      if (image) {
        return (
          <div className="flex" key="1">
            <img style={imageStyle} src={image} alt="image" />
          </div>
        );
      } else {
        return (
          <div
            className="flex"
            style={{
              margin: 20,
            }}
          >
            No image set for this offer
          </div>
        );
      }
    };

    const profileImg = (
      <svg viewBox="0 0 24 24" data-name="Calque 1" focusable="false">
        <circle cx="12" cy="12" fill="#cad1d9" r="12" />
        <circle cx="12" cy="10.26" fill="#a8b4c0" r="4.73" />
        <path
          d="M12 16.64a8.67 8.67 0 0 0-7.73 4.53 12 12 0 0 0 15.46 0A8.67 8.67 0 0 0 12 16.64z"
          fill="#a8b4c0"
        />
      </svg>
    );
    return (
      <div className="wrapper">
        <section className="section-offer">
          <div className="section-main">
            <div className="section-card">
              {imageOffer()}
              <div className="section-card-body">
                <h1>{title}</h1>
                <span>
                  {price}
                  &nbsp;€
                </span>
                <div>{format(new Date(created_at), 'PPPP')}</div>
              </div>
            </div>
            <div className="section-description">
              <div>Description</div>
              <p>{description}</p>
            </div>
          </div>
          <div className="section-sidebar">
            <div className="contact-card">
              <div>{profileImg}</div>
              <div>
                <div className="user">{creator.account.username}</div>
                <div className="phone">{creator.account.phone}</div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }

  render() {
    return this.renderSection();
  }
}

export default Offer;
