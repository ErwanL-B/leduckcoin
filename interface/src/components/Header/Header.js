import React from 'react';
import Button from './Button/Button';
import { ReactComponent as Logo } from '../../assets/img/leduckcoin.svg';
import { ReactComponent as MenuBtn } from '../../assets/img/menu-button.svg';
import { ReactComponent as CloseBtn } from '../../assets/img/close-button.svg';

import {
  ROUTE_SIGNUP,
  ROUTE_LOGIN,
  ROUTE_PROFILE,
  ROUTE_PUBLISH,
  ROUTE_OFFERS,
} from '../../constant/routes';

import './Header.css';

function Header(props) {
  const { token, username, toggleMenu, isToggle, logOut: propsLogOut } = props;

  let renderNav;

  const logOut = (
    <div className="flex">
      <div style={{ width: '130px' }}>
        <Button to={ROUTE_LOGIN} toggleMenu={toggleMenu}>
          Se connecter
        </Button>
      </div>
      <div style={{ width: '160px' }}>
        <Button to={ROUTE_SIGNUP} toggleMenu={toggleMenu}>
          Créer un compte
        </Button>
      </div>
    </div>
  );

  const logIn = (
    <div className="flex">
      <div className="flex">
        <Button to={ROUTE_PROFILE} toggleMenu={toggleMenu}>
          Bienvenue {username}
        </Button>
      </div>
      <div style={{ width: '250px' }}>
        <button type="button" onClick={() => propsLogOut()}>
          Se déconnecter
        </button>
      </div>
    </div>
  );

  if (token) {
    renderNav = logIn;
  } else {
    renderNav = logOut;
  }

  return (
    <header className="header">
      <div className={`wrapper flex ${isToggle ? 'flex-xs' : ''}`}>
        <div>
          <Button to={ROUTE_OFFERS}>
            <Logo />
          </Button>
          <menu
            id="menu"
            className="show-xs"
            onClick={() => {
              toggleMenu();
            }}
          >
            {isToggle ? <CloseBtn /> : <MenuBtn />}
          </menu>
        </div>
        <div className={`nav ${isToggle ? 'menu-open' : 'hidden-xs'}`}>
          <Button to={ROUTE_PUBLISH} toggleMenu={toggleMenu}>
            Déposer une annonce
          </Button>
        </div>
        <div className="account-panel">{renderNav}</div>
      </div>
    </header>
  );
}

export default Header;
