import React from 'react';
import { Link } from 'react-router-dom';
import LinesEllipsis from 'react-lines-ellipsis';

import './Card.css';

import { ROUTE_OFFER } from '../../constant/routes';

const card = ({
  id,
  title,
  price,
  description,
  created_at,
  image,
  isDelete,
  deleteOffer,
  dataTestId,
}) => {
  let imageToDisplay;

  if (image) {
    if (image.length > 0) {
      imageToDisplay = image;
    }
  }

  const style = {
    backgroundImage: `url(${imageToDisplay})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
  };

  let deleteAd;

  if (isDelete) {
    deleteAd = (
      <span
        role="button"
        id="delete-ad"
        onClick={() => {
          deleteOffer(id);
        }}
      >
        ⤫
      </span>
    );
  }

  return (
    <li data-testid={dataTestId || 'card'} className="card">
      {deleteAd}
      <Link to={`${ROUTE_OFFER}/${id}`}>
        <div className="card-body">
          <div className="card-img">
            <div className="img" style={style} />
          </div>
          <div className="card-content">
            <div className="card-top">
              <h3 className="card-title">{title}</h3>
              <span className="card-price">
                {price}
                &nbsp;€
              </span>
            </div>
            <div className="card-bottom" />
            <LinesEllipsis
              text={description}
              maxLine="1"
              ellipsis="..."
              trimRight
              basedOn="letters"
            />
            <span className="date-fns">
              {new Date(created_at).toLocaleDateString('fr')}
            </span>
          </div>
        </div>
      </Link>
    </li>
  );
};

export default card;
