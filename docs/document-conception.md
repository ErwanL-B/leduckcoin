# Document de Conception - Leduckcoin

Leduckcoin est un site internet permettant la mise en ligne d'annonces de canards.
Les utilisateurs peuvent visiter le site afin de trouver le canard parfait.

## Exigences Fonctionnelles et non fonctionnelles

Les exigences fonctionnelles de l'API sont les suivantes. L'API doit :

- Être capable d'afficher la liste des offres disponibles.
- Être capable d'ajouter une nouvelle offre.
- Être capable de modifier une offre.
- Être capable de supprimer une offre.
- Être capable d'ajouter un compte pour un nouvel utilisateur.
- Être capable d'authentifier un utilisateur et de garder sa session active.

Les exigences non fonctionnelles sont les suivantes. L'API sera jugée sur :

- Sa capacité à gérer les offres.
- Sa capacité à gérer les utilisateurs.
- Le niveau de sécurité mis en place pour les communications et mécanismes de stockage des données

Les différentes exigences peuvent influencer l'aspect de l'API ainsi que ces performances.
Il sera préféré une API lente mais fonctionnelle et sécurisée, plutôt que rapide mais non fonctionnelle.

---

## Cas d'utilisation de l'API

L'API est libre d'accès par tous publics. L'API doit afficher la liste des offres disponibles, sans aucune authentification.
L'API doit pouvoir ajouter, modifier ainsi que supprimer une offre.
L'API permet d'ajouter un compte utilisateur.
Un utilisateur est capable de voir ses annonces publiées ainsi que de les modifier et les supprimer.

## Services mis en place

Différents services respectant l'architecture micro-services sont mis en place :
  - Une IHM
  - Un serveur d'accès
  - Une base de données

L'IHM est une interface en langage JavaScript incluant le framwork React. Cette interface permet à l'utilisateur d'envoyer des requêtes au serveur d'accès. Aucun lien direct entre l'interface et la base de données ne doit exister.

Le serveur d'accès est une application permettant de recevoir, de traiter puis de répondre à son appelant.
Le serveur d'accès doit être le seul dispositif capable de communiquer avec la base de données.
Le langage de programmation Rust a était choisi pour le développement du serveur d'accès. Ce langage a l'avantage de :
  - Rendre l'application plus légère en éliminant les composants non nécessaires
  - Rendre l'application plus sécurisée

Afin d'aider le développement de l'IHM, un POC (proof of concept) en langage NodeJs peut être fait pour le développement du serveur d'accès.

La base de donnée choisie pour ce projet est une base de donnée NoSQL : MondoDB.
Elle contiendra deux collections :
  - offers
  - users

La version 4.4.18 de MondoDB sera utilisée pour ce projet. Les versions récentes (à partir de la version 5) demandent une configuration CPU concernant les instructions AVX. La version 4.4.18 de MondoDB a été testée et approuvée. Elle ne requiert pas l'obligation des instructions AVX.

Une base de donnée non relationnelle a était choisie pour ce projet car des documents, lourds et ne disposant pas toujours de la même structure, seront amenés à être enregistrés. Plusieurs champs des offres sont amenés à ne pas être définis, une base de donnée non relationnel est donc recommandé pour ce type de traitement de donnée.

Détails d'une offre :
  - id              (Identifiant unique de l'offre, permet sa récupération unitaire)
  - title           (Titre de l'offre)
  - price           (Prix de l'offre)
  - type            (Type de canard)
  - gender          (Genre du canard)
  - color           (Couleur du canard)
  - description     (Description de l'offre)
  - image           (Texte brut de l'image de l'offre)
  - creator         (Créateur de l'offre)
    - account       (Éléments d'identification du créateur de l'offre)
        - username  (Nom de l'utilisateur)
        - email     (Email de l'utilisateur)
        - phone     (Numéro de téléphone de l'utilisateur)

Les éléments de contacte du propriétaire de l'annonce sont renseignés avec l'offre. Aucun lien direct n'est associé entre un utilisateur et une offre afin d'augmenter la sécurité des données.

Détails d'un utilisateur :
  - username  (Nom de l'utilisateur)
  - email     (Email de l'utilisateur)
  - password  (Mot de passe de l'utilisateur)
  - phone     (Numéro de téléphone de l'utilisateur)

MondoDB ajoute automatiquement un identifiant (_id) à tous les documents. Cet identifiant peut servir de token lors de l'authentification d'un utilisateur.

Aucun mot de passe ne doit être directement visible à partir de la base de donnée. Un hashage du mot de passe est obligatoire avant sa sauvegarde dans la base de donnée.


## API - Leduckcoin

Leduckcoin est une API REST principalement codée en langage NodeJs. Elle inclue deux composants : une interface utilisateur (IHM) et un serveur d'accès.

L'IHM est codée en JavaScript incluant le framework React. L'utilisation de React améliore la réactivité et la fluidité de l'application.
L'interface est portative et communique avec le serveur d'accès via des requête REST.

Le serveur d'accès est développé en deux parties :
  - Un POC (proof of concept) codé en langage NodeJs afin de facilité le développement des autres micro-services
  - Une Traduction en langage Rust pour rentre le serveur d'accès plus légé et sécurisé

L'application est en constante écoute pour recevoir les demandes de l'interface.
Le serveur d'accès reçoit, traite et répond aux requête REST reçues.
Le serveur d'accès communique via le protocol HTTPS pour sécuriser les échanges.

## Requêtes REST

Plusieurs requêtes REST sont mises en place pour établir une communication entre l'interface et le serveur d'accès.

  * **GET**

*/api/offers* - Permet la récupération de toutes les offres disponibles. Aucun paramètre ou authentification n'est requis pour cette requête.

Un ensemble de filtres ainsi qu'un tri sont configurables pour cette requête :
  - title : (String)
  - priceMin : (integer)
  - priceMax : (integer)
  - sort : ('price-asc', 'price-desc', 'date-asc', 'date-desc')

La réponse de cette requête contient un tableau au format JSON, de toutes les offres disponibles.
Si un filtre est appliqué, les annonces retournées seront filtrées puis triées en fonction de la demande.

*/api/offer* - Permet la récupération unitaire d'annonce. Cette requête requiert :
  - l'identifiant de l'offre (Integer)

La réponse de cette requête contient un tableau au format JSON, contenant l'annonce demandée.

  * **POST**

*/api/log_in* - Permet l'authentification d'un utilisateur. Cette requête requiert :
  - Un email (String)
  - Un mot de passe (String)

La réponse de cette requête contient le token lié au compte utilisateur connecté.

*/api/sign_up* - Permet l'ajout d'un nouveau compte utilisateur. Cette requête requiert :
  - Un pseudo (String)
  - Un email (String)
  - Un mot de passe (String)
  - Un numéro de téléphone (String)

La réponse de cette requête contient le token lié au compte utilisateur connecté.

*/api/send_offer* - Permet l'ajout d'une nouvelle offre. Cette requête requiert :
  - Un identifiant unique 
  - Un titre (String)
  - Un prix (Integer)
  - Le pseudo du propriétaire (String)
  - L'email du propriétaire (String)
  - Le numéro de téléphone du propriétaire (String)

Les champs suivants sont optionnels :
  - Un type de canard (String)
  - Le genre du canard (String)
  - Une couleur (String)
  - Une description (String)
  - Une image (String)

La réponse de cette requête contient l'identifiant unique généré par MondoDB de l'offre nouvellement créée.

*/api/profile* - Permet la récupération des informations liés à un utilisateur. Cette requête requiert : 
  - Un token lié à un utilisateur (String)

La réponse de cette requête contient le username ainsi que l'email du compte connecté.
La réponse est accompagnée de la liste de toutes les offres liées au compte utilisateur connecté.

  * **DELETE**

*/api/delete_offer* - Permet la suppression d'une offre. Cette requête requiert :
  - Le token lié au compte utilisateur (String)
  - L'identifiant unique de l'offre (String)

La réponse de cette requête est un élément au format JSON, contenant le nombre d'éléments supprimés

  * **PUT**

*/api/modify_offer* - Permet la modification d'une offre. Cette requête requiert :
  - Le token lié au compte utilisateur (String)
  - L'identifiant unique de l'offre (String)

Les champs suivants sont optionnels :
  - Le nouveau titre de l'offre (String)
  - Le nouveau prix de l'offre (Integer)
  - Un nouveau type de canard (String)
  - Le nouveau genre du canard (String)
  - La nouvelle couleur du canard (String)
  - La nouvelle description (String)
  - La nouvelle image (String)

La réponse de cette requête est un élément au format JSON, contenant le nombre d'éléments modifiés

## Gestion du projet et de son déploiement

Ce projet a pour objectif d'être déployé sur un serveur Ubuntu de production.
Pour réaliser cela, l'IHM, le serveur d'accès ainsi qu'une base de donnée MondoDB doivent être conteneurisés.
Ces trois images seront versionnées sur un dépôt Git permettant par la suite leurs récupération distante.
La base de données peut ne pas être versionnée sur le dépôt Git si l'image est disponible via DockerHub.

Via l'outil Ansible, les trois conteneurs doivent être déployés de manière automatique sur le serveur de production.

Pour gestionner le projet, une plannification des tâches ainsi qu'un système de ticket sera mis en place avec l'outil Jira.

Un dépôt Git doit être mis en place pour ce projet. Deux branches seront présentes : 
  - La branche Main (Version officielle du projet)
  - La branch Dev (Version en cours de développement)

Une pipeline sera mise en place pour ce projet, grâce à l'outil Jenkins. Jenkins devra être en capacité de vérifier le code, de le compiler ainsi que de l'exporter (en conteneur) sur le dépôt Git.

## Sécurité liée au projet

La sécurisation du projet est mise en place par plusieurs dispositifs :
  - Protocole HTTPS entre l'IHM et le serveur d'accès
  - Cryptage des mots de passe par le serveur d'accès
  - Authentification du serveur d'accès auprès de la base de données MondoDB

Lors du POC, tous les dispositifs de sécurisation doivent être mis en place grâce à des bibliothèques spécifiques.
Lors de la traduction de l'application en langage Rust, les outils natifs doivent être prioritaires pour la sécurisation de l'application.

Le langage NodeJs permet, grâce à des bibliothèques, de mettre en place la sécurisation des échanges, des communications ainsi que de la gestion des données. Ce langage permet de rapidement mettre en place une application incluant une sécurisation minimale.

Le langage Rust permet, grâce à des bibliothèques ainsi que des outils natifs, de mettre en place une sécurité importante pour les communications et la gestion des données. De plus, le langage Rust possède beaucoup d'exigence en ce qui concerne la sécurité et les performances, ce qui est adéquate avec ce projet.

## Arbres principaux

application/
├── log/                       (log du serveur d'accès)
│   ├── build/                     (build du serveur d'accès)
│   ├── src/
│   │   ├── fonctions/             (fonctions importantes du serveur d'accès)
│   │   │   ├── mongo/             (fonctions relatives à MondoDB)
│   │   │   ├── offers/            (fonctions relatives aux offres)
│   │   │   ├── users/             (fonctions relatives aux utilisateurs)
│   │   ├── ressources/
│   │   │   ├── constantes/        (Toutes les constantes du serveur d'accès)
│   │   ├── routes/                (Définition des routes - requêtes REST)
│   │   ├── acces_server.js
│   │   ├── api.controlerGet/      (Contrôler des requêtes GET)
│   │   ├── api.controlerPost/     (Contrôler des requêtes POST)
│   │   ├── api.controlerDelete/   (Contrôler des requêtes Delete)
│   │   ├── api.controlerPut/      (Contrôler des requêtes Put)
│   ├── Dockerfile
│   ├── package.json

interface/
│   ├── build/                     (Build de l'interface)
│   ├── public/
│   │   ├── favicon.ico           (Logo de l'interface)
│   │   ├── index.html            (Page de lancement de l'interface)
│   │   ├── manifest.json         (Manifest de l'interface)
│   ├── src/
│   │   ├── assets/                (Assets de l'interface - images)
│   │   ├── components/            (Composent de l'interface - React)
│   │   │   ├── Card/              (Offre)
│   │   │   ├── Error/             (Erreur)
│   │   │   ├── Filters/           (Filtres de recherche)
│   │   │   ├── Header/            (En-tête)
│   │   │   ├── Loading/           (Chargement)
│   │   │   ├── Pagination/        (Pagination des offres)
│   │   ├── constant/              (Constantes de l'interface)
│   │   ├── containers/            (Fonctions de l'interface)
│   │   │   ├── LogIn/             (Connexion d'un utilisateur)
│   │   │   ├── Offer/             (Détail d'une offre)
│   │   │   ├── Offers/            (Page principal - Liste toutes les offres)
│   │   │   ├── Profile/           (Profile utilisateur - regroupe les offres propriétaires)
│   │   │   ├── Publish/           (Publication d'une nouvelle offre)
│   │   │   ├── SignUp/            (Enregistrement d'un nouvel utilisateur)
│   │   ├── App.js                (Application - contient les composents)
│   │   ├── index.js              (JS de lancement)
│   ├── Dockerfile
│   ├── package.json

deploiement/
│   ├── Dockerfile          (Définition des conteneurs à importer pour le déploiement)
│   ├── docker-compose.yml  (Définition du comportement des conteneurs importés)
│   ├── insomnia.json       (Jeu de test de requêtes REST via Insomnia)
