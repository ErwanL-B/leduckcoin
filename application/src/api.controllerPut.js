import logger from './logger.js';

import { mongoPutOffer } from './functions/offers/offers.js';

export const modifyOffer = async (req, res) => {
	logger.info(`[api] PUT ${req.originalUrl} requested`);
	try {
		if (
			req?.body?.id &&
			req.body.title &&
			req.body.price &&
			req.body.type &&
			req.body.color &&
			req.body.gender &&
			req.body.description &&
			req.body.created_at
		) {
			await mongoPutOffer(req.body).then(response => {
				res.status(200).send(response);
			});
		} else {
			throw new Error('One element or more missing');
		}
	} catch (err) {
		if (err) {
			logger.info(`[api] error : ${err}`);
			return res.status(500).send({ message: `${err}` });
		}
	}
};
