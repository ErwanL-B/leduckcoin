/************************ API ROUTE **************************/
export const OFFERS = '/offers';
export const OFFER = '/offer';
export const SIGNUP = '/sign_up';
export const LOGIN = '/log_in';
export const SENDOFFER = '/send_offer';
export const MODIFYOFFER = '/modify_offer';
export const DELETEOFFER = '/delete';
export const PROFILE = '/profile';

/************************ CONNEXION **************************/

export const REST_URL = 'http://127.0.0.1:8000/';
export const WEB_PORT = 8845;

/************************ DataBase **************************/

//mongodb://[username:password@]host1[:port1][,...hostN[:portN]][/[defaultauthdb][?options]]

// const username = encodeURIComponent('leduckcoin');
// const password = encodeURIComponent('leduckcoin');
// const authMechanism = 'DEFAULT';

// const host = '127.0.0.1:27017/';
//export const MONGO_DB_URI = `mongodb://${username}:${password}@${host}?authMechanism=${authMechanism}`;
//export const MONGO_DB_URI = 'mongodb://127.0.0.1:27017/';
export const MONGO_DB_URI = 'mongodb://10.20.1.63:27017/';
