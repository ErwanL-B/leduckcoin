export const default_value = {
    offers : [
        {
            "id" : '1',
            "title" : 'Canard1',
            "price": 100,
            "type":'canard raccoucit',
            "color":'Orange',
            "gender":"male",
            "description" : "Je suis une description",
            "created_at" : "10/10/2000",
            "image" : "",
            "creator": 
                { "account" : 
                        {
                            "username" : "Erwan",
                            "phone" :"0713245678"
                        }
                }
        },
        {
            "id" : '2',
            "title" : 'Canard2',
            "price": 100,
            "type":'canard raccoucit',
            "color":'Orange',
            "gender":"male",
            "description" : "Je ne suis pas une description",
            "created_at" : "10/10/2000",
            "image" : "",
            "creator": 
                {
                    "account" : 
                        {
                            "username" : "Erwan",
                            "phone" :"0713245678"
                        }
                }
        }
    ]
};