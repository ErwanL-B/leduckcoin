import logger from './logger.js';

import { mongoGetOffers } from './functions/offers/offers.js';

export const getOffers = async (req, res) => {
	logger.info(`[api] GET ${req.originalUrl} requested`);
	try {
		const queryStringParameters = {};
		if (req.query) {
			if (req.query.skip) {
				queryStringParameters.skip = req.query.skip;
			}
			if (req.query.limit) {
				queryStringParameters.limit = req.query.limit;
			}
			if (req.query.title) {
				queryStringParameters.title = req.query.title;
			}
			if (req.query.sort) {
				queryStringParameters.sort = req.query.sort;
			}
			if (req.query.priceMin) {
				const price =
					typeof req.query.priceMin == 'number'
						? req.query.priceMin
						: parseInt(req.query.priceMin);
				queryStringParameters.priceMin = price;
			}
			if (req.query.priceMax) {
				const price =
					typeof req.query.priceMax == 'number'
						? req.query.priceMax
						: parseInt(req.query.priceMax);
				queryStringParameters.priceMax = price;
			}
		} else {
			throw new Error('No data filter set');
		}

		await mongoGetOffers(queryStringParameters).then(response => {
			res.status(200).send(response);
		});
	} catch (err) {
		if (err) {
			logger.info(`[api] error : ${err}`);
			return res.status(500).send({ message: `${err}` });
		}
	}
};

export const getOffer = async (req, res) => {
	logger.info(`[api] GET ${req.originalUrl} requested`);
	try {
		const queryStringParameters = {
			id: req.query?.id,
		};
		await mongoGetOffers(queryStringParameters).then(offers => {
			let offer_response = undefined;
			offers.forEach(offer => {
				if (offer.id == queryStringParameters.id) {
					offer_response = offer;
				}
			});
			res.status(200).send(offer_response);
		});
	} catch (err) {
		if (err) {
			logger.info(`[api] error : ${err}`);
			return res.status(500).send({ message: `${err}` });
		}
	}
};
