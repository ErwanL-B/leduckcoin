import logger from './logger.js';

import { mongoDeleteOffer, mongoGetOffers } from './functions/offers/offers.js';

export const deleteOffer = async (req, res) => {
	var offerExist = new Boolean(false);
	logger.info(`[api] DELETE ${req.originalUrl} requested`);
	try {
		await mongoGetOffers().then(offers => {
			offers.forEach(offer => {
				if (offer.id == req.query.id && offer._id == req.query.token) {
					offerExist = true;
				}
			});
			if (offerExist) {
				const offer = {
					id: req.query.id,
				};

				mongoDeleteOffer(offer).then(response => {
					res.status(200).send(response);
				});
			} else {
				throw new Error("This ID doesn't exist");
			}
		});
	} catch (err) {
		if (err) {
			logger.info(`[api] error : ${err}`);
			return res.status(500).send({ message: `${err}` });
		}
	}
};
