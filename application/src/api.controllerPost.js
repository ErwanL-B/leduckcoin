import logger from './logger.js';

import { mongoPostOffer, mongoGetOffers } from './functions/offers/offers.js';
import { mongoPostUser, mongoGetUsers } from './functions/users/users.js';
import { compare, genSalt, hash } from 'bcrypt';

export const signUp = async (req, res) => {
	logger.info(`[api] POST ${req.originalUrl} requested`);
	try {
		await mongoGetUsers().then(users => {
			users.forEach(user => {
				if (user.email) {
					if (user.email == req.query.email) {
						throw new Error('This email already exist');
					}
				}
			});

			let passwordEncrypt = null;

			genSalt(10, (err, salt) => {
				if (err) throw new Error(err);

				hash(req.query.password, salt, (err, hash) => {
					passwordEncrypt = hash;

					if (passwordEncrypt == null) {
						logger.info(`[api] error : ${err.message}`);
						return res.status(500).send({ message: `${err.message}` });
					}

					const user = {
						email: req.query.email,
						username: req.query.username,
						password: passwordEncrypt,
					};

					if (req.query.phone) {
						user.phone = req.query.phone;
					}

					if (!user.username || !user.email || !user.password) {
						const message = 'User registry error';
						logger.info(`[api] error : ${message}`);
						return res.status(500).send({ message: `${message}` });
					}

					mongoPostUser(user).then(response => {
						const userAuthentify = {
							token: response.insertedId,
							email: user.email,
							username: user.username,
						};
						res.status(200).send(userAuthentify);
					});
				});
			});
		});
	} catch (err) {
		if (err) {
			logger.info(`[api] error : ${err.message}`);
			return res.status(500).send({ message: `${err.message}` });
		}
	}
};

export const sendOffer = async (req, res) => {
	logger.info(`[api] POST ${req.originalUrl} requested`);

	try {
		let newId = 1;
		let idUsed = Array();
		await mongoGetOffers().then(offers => {
			if (offers?.statusCode != 400) {
				offers?.forEach(offer => {
					if (offer.id) {
						idUsed.push(offer.id);
					}
				});
			}
		});

		for (let i = 0; i < idUsed.length; i++) {
			if (idUsed.indexOf(newId) != -1) {
				newId++;
			} else {
				break;
			}
		}

		const created_at = new Date();

		const price =
			typeof req.query.price == 'number'
				? req.query.price
				: parseInt(req.query.price);
		const offer = {
			id: newId,
			title: req.query.title,
			price,
			type: '',
			color: '',
			gender: '',
			description: '',
			image: '',
			created_at,
		};

		if (req.query.description) {
			offer.description = req.query.description;
		}
		if (req.query.type) {
			offer.type = req.query.type;
		}
		if (req.query.color) {
			offer.color = req.query.color;
		}
		if (req.query.gender) {
			offer.gender = req.query.gender;
		}
		if (req.body.image) {
			offer.image = req.body.image;
		}
		await mongoGetUsers().then(users => {
			users.forEach(user => {
				if (user._id?.toString() == req.query.token) {
					const account = {
						username: user.username,
						email: user.email,
					};

					offer.creator = { account };
					if (user.phone) {
						offer.creator.account.phone = user.phone;
					}
					mongoPostOffer(offer).then(response => {
						res.status(200).send(response);
					});
				}
			});
		});
	} catch (err) {
		if (err) {
			logger.info(`[api] error : ${err}`);
			return res.status(500).send({ message: `${err}` });
		}
	}
};

export const connectUser = async (req, res) => {
	logger.info(`[api] POST ${req.originalUrl} requested`);
	try {
		let userEmail = null;
		await mongoGetUsers().then(users => {
			users.forEach(user => {
				if (user._id?.toString() == req.query.token) {
					userEmail = user.email;
				}
			});

			if (userEmail) {
				const queryStringParameters = { creator: userEmail };

				mongoGetOffers(queryStringParameters).then(offers => {
					res.status(200).send(offers);
				});
			}
		});
	} catch (err) {
		if (err) {
			logger.info(`[api] error : ${err.message}`);
			return res.status(500).send({ message: `${err.message}` });
		}
	}
};

export const login = async (req, res) => {
	logger.info(`[api] GET ${req.originalUrl} requested`);
	try {
		if (!req.query?.email || !req.query?.password) {
			throw new Error('Login element missing');
		}

		const queryStringParameters = {
			email: req.query?.email,
		};

		await mongoGetUsers(null, queryStringParameters).then(user => {
			try {
				compare(
					req.query.password,
					user[0].password,
					(err, isPasswordMatch) => {
						if (!isPasswordMatch) {
							const message =
								"Erreur d'authentification, Veuillez vérifiez vos identifiants";
							logger.info(`[api] error : ${message}`);
							return res.status(500).send({ message: `${message}` });
						}
						const token = user[0]._id;
						const email = user[0].email;
						const username = user[0].username;

						const response = { token, email, username };
						res.status(200).send(response);
					}
				);
			} catch (err) {
				throw new Error('Login error');
			}
		});
	} catch (err) {
		if (err) {
			logger.info(`[api] error : ${err}`);
			return res.status(500).send({ message: `${err}` });
		}
	}
};
