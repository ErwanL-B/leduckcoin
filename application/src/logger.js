import { createRequire } from 'module';

const require = createRequire(import.meta.url);
const fs = require('fs-extra');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;
require('winston-daily-rotate-file');

/** Directory path you want to set */
const logDir = 'Access_server_log';

if (!fs.existsSync(logDir)) {
	/** Create the directory if it does not exist */
	fs.mkdirSync(logDir);
}

const transport = new transports.DailyRotateFile({
	filename: `${logDir}/webserver-logs-%DATE%.log`,
	maxSize: '10m',
	maxFiles: '4',
});

const exceptionsTransport = new transports.DailyRotateFile({
	filename: `${logDir}/webserver-exceptions-%DATE%.log`,
	maxSize: '10m',
	maxFiles: '4',
});

const myFormat = printf(({ level, message, timestamp }) => {
	return `${timestamp} ${level}: ${message}`;
});

const logger = createLogger({
	level: 'info',
	exitOnError: false,
	format: combine(timestamp(), myFormat),
	transports: [transport],
	exceptionHandlers: [exceptionsTransport],
});

if (process.env.NODE_ENV !== 'production') {
	logger.add(
		new transports.Console({
			format: combine(timestamp(), myFormat),
			encoding: 'utf8',
		})
	);
}

export default logger;
