/****************** V1.2.2 *********************/
import logger from './logger.js';
import path from 'node:path';

import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import https from 'https';
import fs from 'fs';

import { fileURLToPath } from 'url';
import { dirname } from 'path';

import { WEB_PORT } from './ressource/constants/connexion.js';
import apiRouter from './routes/api.routes.js';
import fileUpload from 'express-fileupload';
//import { connected } from 'process';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

global.__basedir = __dirname;

const app = express();

/** set port to listen for requests */
const port = process.env.PORT || WEB_PORT;

try {
	app.use(express.json());

	/* ------------- cyber security ------------ */
	app.use(helmet());
	app.use(
		cors({
			origin: true,
		})
	);
	app.use(fileUpload());
	app.use(express.urlencoded({ extended: true }));
	app.set('trust proxy', 1); // trust first proxy

	/* ----------- routes ----------- */
	app.use('/api', apiRouter);

	/* --------------------------- REST Error management --------------------------- */

	app.use((req, res, next) => {
		next({
			status: 404,
			message: 'Not Found',
		});
	});

	app.use((err, req, res, next) => {
		if (err.status === 404) {
			logger.error(
				`[api] La requête demandée n\'existe pas : ${req.path}, erreur 404 retournée`
			);
			res.status(404);
			return res.json(err);
		}

		if (err.status === 500) {
			logger.error(`[api] erreur 500 retournée`);
			return res.status(500);
		}
	});

	//--------------------------- Web Client to NodeJS - REST Get API ---------------------------

	// export const httpServer = app.listen(port, () => {
	// 	logger.info(`[api] http server is running on port ${port}.`);
	// });
} catch (err) {
	logger.info(`[api] Error launch access server : ${err}.`);
}

export const httpServer = https
	.createServer(
		{
			key: fs.readFileSync('./src/ressource/keys/key.pem'),
			cert: fs.readFileSync('./src/ressource/keys/cert.pem'),
		},
		app
	)
	.listen(port, () => {
		logger.info(`[api] https server is running on port ${port}.`);
	});
