import { MONGO_DB_URI } from '../../ressource/constants/connexion.js';
import { connectToCluster } from '../mongo/connect.js';

/**************** Mongo GET  *****************/

export async function mongoGetUsers(_id = null, queryStringParameters = null) {
	let mongoClient;
	try {
		/** Connect to database */
		mongoClient = await connectToCluster(MONGO_DB_URI);
		const db = mongoClient.db('users');
		const mongoCollection = db.collection('users');

		if (_id) {
			/** Get offer by id */
			return await mongoCollection.find({ _id: parseInt(_id) }).toArray();
		} else if (queryStringParameters?.email) {
			return await mongoCollection
				.find({
					email: queryStringParameters.email,
				})
				.toArray();
		} else {
			return await mongoCollection.find().toArray();
		}
	} finally {
		await mongoClient.close();
	}
}

/**************** Mongo POST  *****************/

export async function mongoPostUser(user) {
	let mongoClient;
	try {
		/** Connect to database */
		mongoClient = await connectToCluster(MONGO_DB_URI);
		const db = mongoClient.db('users');
		return await db.collection('users').insertOne(user);
	} finally {
		await mongoClient.close();
	}
}
