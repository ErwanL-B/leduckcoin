import { MongoClient } from 'mongodb';
import { MONGO_DB_URI } from '../../ressource/constants/connexion.js';

export async function connectToCluster() {
	let mongoClient;
	try {
		mongoClient = new MongoClient(MONGO_DB_URI);
		console.log('Connecting to MongoDB...');
		await mongoClient.connect();
		console.log('Successfully connected to MongoDB');
		return mongoClient;
	} catch (error) {
		console.error('Connection to MongoDB failed!', error);
		console.log(error.toString());
	}
}
