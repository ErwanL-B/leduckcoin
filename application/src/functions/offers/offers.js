import { connectToCluster } from '../mongo/connect.js';
import { buildFilters, buildSortOrder } from './utils.js';

/**************** Mongo GET  *****************/

export async function mongoGetOffers(queryStringParameters = {}) {
	let mongoClient;
	try {
		/** Get filter */
		const skip = queryStringParameters.skip ? queryStringParameters.skip : 0;
		const limit = queryStringParameters.limit ? queryStringParameters.limit : 0;

		const { id, creator, priceMin, priceMax, title, sort } =
			queryStringParameters;

		const filter = buildFilters(title, priceMin, priceMax);
		const sortOrder = buildSortOrder(sort);

		/** Connect to database */
		mongoClient = await connectToCluster();
		const db = mongoClient.db('offers');
		const mongoCollection = db.collection('offers');

		/** Get offer by id */
		if (id) {
			return await mongoCollection.find({ id: parseInt(id) }).toArray();
		}

		if (creator) {
			return await mongoCollection
				.find({
					'creator.account.email': creator,
				})
				.toArray();
		}

		/** Get data */
		return await mongoCollection
			.find(filter)
			.sort(sortOrder)
			.skip(parseInt(skip))
			.limit(parseInt(limit))
			.toArray();
	} finally {
		await mongoClient.close();
	}
}

/**************** Mongo POST  *****************/

export async function mongoPostOffer(offer) {
	let mongoClient;
	try {
		/** Connect to database */
		mongoClient = await connectToCluster();
		const db = mongoClient.db('offers');

		return await db.collection('offers').insertOne(offer);
	} finally {
		await mongoClient.close();
	}
}

/**************** Mongo PUT  *****************/

export async function mongoPutOffer(offer) {
	let mongoClient;
	try {
		/** Connect to database */
		mongoClient = await connectToCluster();
		const db = mongoClient.db('offers');

		return await db.collection('offers').updateMany(
			{ id: offer.id },
			{
				$set: {
					title: offer.title,
					price: offer.price,
					type: offer.type,
					color: offer.color,
					gender: offer.gender,
					description: offer.description,
					created_at: offer.created_at,
					image: offer.image,
				},
			}
		);
	} finally {
		await mongoClient.close();
	}
}

/**************** Mongo Delete  *****************/

export async function mongoDeleteOffer(offer) {
	let mongoClient;
	try {
		/** Connect to database */
		mongoClient = await connectToCluster();
		const db = mongoClient.db('offers');
		return await db.collection('offers').deleteOne({ id: parseInt(offer.id) });
	} finally {
		await mongoClient.close();
	}
}
