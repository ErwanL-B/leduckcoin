import express from 'express';

import * as controllerGet from '../api.controllerGet.js';
import * as controllerPost from '../api.controllerPost.js';
import * as controllerPut from '../api.controllerPut.js';
import * as controllerDelete from '../api.controllerDelete.js';

import {
	OFFERS,
	OFFER,
	SIGNUP,
	SENDOFFER,
	MODIFYOFFER,
	DELETEOFFER,
	PROFILE,
	LOGIN,
} from '../ressource/constants/connexion.js';

const router = express.Router();

/** default get request */
router.get('/', (req, res) => {
	res.json({ status: 200, message: 'GET Welcome.' });
});

/** Offers get request */
router.get(OFFERS, controllerGet.getOffers);

/** Offer get request */
router.get(OFFER, controllerGet.getOffer);

/************************** POST **************************/

/** default post request */
router.post('/', (req, res, next) => {
	res.json({ status: 200, message: 'POST Welcome.' });
});

/** Add offer */
router.post(SENDOFFER, controllerPost.sendOffer);

/** Sign up */
router.post(SIGNUP, controllerPost.signUp);

/** Profile */
router.post(PROFILE, controllerPost.connectUser);

/** login */
router.post(LOGIN, controllerPost.login);

/************************** PUT **************************/

/** default post request */
router.put('/', (req, res, next) => {
	res.json({ status: 200, message: 'PUT Welcome.' });
});

/** Modify offer */
router.put(MODIFYOFFER, controllerPut.modifyOffer);

/************************** DELETE **************************/

/** default post request */
router.delete('/', (req, res, next) => {
	res.json({ status: 200, message: 'DELETE Welcome.' });
});

/** Modify offer */
router.delete(DELETEOFFER, controllerDelete.deleteOffer);

export default router;
